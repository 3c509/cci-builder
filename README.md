# cci-builder

## Description:

Ver 1.0  -  8 may 2014
Alex Theodore 
		   

Generates CCI / Raid manager configuration files using a CSV file
and a configuration file. 

This can be used to build horcm*.conf files for:
        ShadowImage, ThinImage, and TrueCopy.  
	
The tool will only build simple L1 copies.  

Should you require more advanced configurations you can use this as a starting point and edit the resultant files from this tool.

As is. No warranty.

## Usage:


### 1) Build CSV file containing volumes

- One line per PVOL/SVOL relationship
- CSV file should be in the following format

group = device group name
src_cu = pvol cu
src_ldev = pvol ldev (number after cu)
dst_cu = svol cu
dst_ldev = svol ldev (number after cu)
src_sn = serial number of array containing pvol
dst_sn = serial number of array containing svol

i.e.

##group,src_cu,src_ldev,dst_cu,dst_ldev,src_sn,dst_sn
sbconsmr1,31,0,21,0,32509,32509
sbconsmr2,31,1,21,1,32509,32509
sbkprdccpdb2,31,2,21,2,32509,32509
sbkprdccpdb2,31,3,21,3,32509,32509

### 2) Edit options.conf

Edit the configuration options appropriate to your use.

### 3) Run the tool


*Program Usage Options*

$ ./build-horcc 
Usage: ./build-horcc -c CSV_FILE [ -f options.conf | -h ]
	-c CSV_FILE             input file containing volumes
  -f options.conf         configuration file to build files

*Example Using Tool*

$ ./build-horcc -c vols.csv -f options.conf
Starting CCI Builder...
	Creating file:   horcm100.conf
  Creating file:   horcm101.conf
  Creating file:   rm_cci_cli_cmds.txt
Done.



